﻿/*
 * The base library for ProbeNet database exports
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace ProbeNet.Export.Database
{
    /// <summary>
    /// This class is used for saving an extended table column configuration. "Extended" means that it is possible
    /// to specify one or more conditions. If the result of these conditions is <c>true</c> the target value will be
    /// stored into the column which has the target column name. Nothing will happen if the conditions evaluate to
    /// <c>false</c>. Of course, you may specify another condition for that case.
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    public class ExtendedTableColumnConfiguration
    {
        /// <summary>
        /// The list of conditions that must be checked
        /// </summary>
        [JsonProperty("conditions")]
        public IList<TableColumnCondition> Conditions { get; set; }

        /// <summary>
        /// The list of target columns where to put values if condition is true.
        /// </summary>
        [JsonProperty("targets")]
        public IList<TableColumnTarget> Targets { get; set; }

        /// <summary>
        /// The name of the column where the target value should be stored
        /// </summary>
        /// <remarks>Can only be set in order to enable reading old configurations</remarks>
        [Obsolete("Use Targets property instead", true)]
        [JsonProperty("targetColumnName")]
        public string TargetColumnName
        {
            set
            {
                InitializeTargets();
                Targets[0].Name = value;
            }
        }

        /// <summary>
        /// The value that should be stored into the target column if condition evaluates to <c>true</c>.
        /// </summary>
        /// <remarks>Can only be set in order to enable reading old configurations</remarks>
        [Obsolete("Use Targets property instead", true)]
        [JsonProperty("targetValue")]
        public string TargetValue
        {
            set
            {
                InitializeTargets();
                Targets[0].Value = value;
            }
        }

        void InitializeTargets()
        {
            if (Targets == null) {
                Targets = new List<TableColumnTarget>();
            }
            if (Targets.Count < 1) {
                Targets.Add(new TableColumnTarget());
            }
        }
    }
}
