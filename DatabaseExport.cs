﻿/*
 * The base library for ProbeNet database exports
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Frank.Database.Connection;
using Internationalization;
using System.Collections.Generic;

namespace ProbeNet.Export.Database
{
    /// <summary>
    /// Base class for all database exports.
    /// </summary>
    public abstract class DatabaseExport : DataExport
    {
        /// <summary>
        /// Construct a new database export object
        /// </summary>
        /// <param name="settingsProvider">Settings provider</param>
        protected DatabaseExport(SettingsProvider settingsProvider)
            : base(settingsProvider)
        {
        }

        /// <summary>
        /// The database export settings
        /// </summary>
        public DatabaseExportSettings Settings
        {
            get;
            protected set;
        }

        /// <summary>
        /// Export the data to the database.
        /// </summary>
        /// <param name="i18n">Internationalization object for translations</param>
        /// <param name="model">The data that should be exported.</param>
        /// <param name="measurementIdsForExport">The measurement Ids from the model that should be exported</param>
        public abstract void Export(
            I18n i18n, MultipleSourcesDataModelBase model, IList<IList<string>> measurementIdsForExport);

        /// <summary>
        /// Test the database connection and the existance of required tables and table columns.
        /// </summary>
        /// <returns><c>true</c> if test was successful, otherwise <c>false</c></returns>
        public abstract bool Test();

        /// <summary>
        /// The database connection.
        /// </summary>
        protected DatabaseConnection Connection
        {
            get;
            set;
        }

        /// <summary>
        /// The table column configuration.
        /// </summary>
        protected TableColumnConfiguration TableColumnConfiguration
        {
            get;
            set;
        }
    }
}
