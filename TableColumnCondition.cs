﻿/*
 * The base library for ProbeNet database exports
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Frank.Database.Connection;
using Frank.Helpers.Json;
using Newtonsoft.Json;

namespace ProbeNet.Export.Database
{
    /// <summary>
    /// Class for saving table column condition configuration
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    public class TableColumnCondition
    {
        /// <summary>
        /// Default constructor.
        /// </summary>
        public TableColumnCondition()
        {
            Key = null;
            Comparison = Comparison.Equal;
            Value = null;
            Conjunction = Conjunction.And;
        }

        /// <summary>
        /// The key is the measurement property which must meet a certain condition
        /// </summary>
        [JsonProperty("key")]
        public string Key { get; set; }

        /// <summary>
        /// The type of comparison that should be used to compare the key with the value
        /// </summary>
        [JsonProperty("comparison")]
        [JsonConverter(typeof(JsonDescriptionEnumConverter<Comparison>))]
        public Comparison Comparison { get; set; }

        /// <summary>
        /// The value that the measurement property must fulfill
        /// </summary>
        [JsonProperty("value")]
        public string Value { get; set; }

        /// <summary>
        /// Defines how this condition is conjuncted to a previous condition in a list.
        /// This property is ignored if this is the first condition in a list.
        /// </summary>
        [JsonProperty("conjunction")]
        [JsonConverter(typeof(JsonDescriptionEnumConverter<Conjunction>))]
        public Conjunction Conjunction { get; set; }

        /// <summary>
        /// Test equality.
        /// </summary>
        /// <param name="a">Object one (left hand side of equal sign)</param>
        /// <param name="b">Object two (right hand side of equal sign)</param>
        /// <returns><c>true</c> if and only if both objects are equal, that means their fields are equal; otherwise <c>false</c></returns>
        public static bool operator ==(TableColumnCondition a, TableColumnCondition b)
        {
            if ((object)a == null && (object)b == null) {
                return true;
            }
            if ((object)a == null || (object)b == null) {
                return false;
            }
            return a.Key == b.Key && a.Comparison == b.Comparison && a.Value == b.Value &&
                a.Conjunction == b.Conjunction;
        }


        /// <summary>
        /// Test inequality.
        /// </summary>
        /// <param name="a">Object one (left hand side of equal sign)</param>
        /// <param name="b">Object two (right hand side of equal sign)</param>
        /// <returns><c>true</c> if and only if both objects are not equal; otherwise <c>false</c></returns>
        public static bool operator !=(TableColumnCondition a, TableColumnCondition b)
        {
            return !(a == b);
        }

        /// <summary>
        /// Test equality of this object and another.
        /// </summary>
        /// <param name="b">The other object</param>
        /// <returns><c>true</c> if and only if both objects are equal, that means their fields are equal; otherwise <c>false</c></returns>
        public override bool Equals(object obj)
        {
            if (obj is TableColumnCondition) {
                return ((TableColumnCondition)obj) == this;
            }
            return base.Equals(obj);
        }

        /// <inherhitdoc/>
        public override int GetHashCode()
        {
            return Key.GetHashCode() ^ Comparison.GetHashCode() ^ Value.GetHashCode() ^ Conjunction.GetHashCode();
        }

        public override string ToString()
        {
            return string.Format("Condition ({0}) {1} {2} {3}", Conjunction, Key, Comparison, Value);
        }
    }
}
