﻿using Newtonsoft.Json;

namespace ProbeNet.Export.Database
{
    /// <summary>
    /// This class is for saving the target of an extended table column condition. The column name define the
    /// name of the target column, the value is the value that should be written.
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    public class TableColumnTarget
    {
        /// <summary>
        /// The name of the column
        /// </summary>
        [JsonProperty("columnName")]
        public string Name { get; set; }

        /// <summary>
        /// The value to be written
        /// </summary>
        [JsonProperty("value")]
        public string Value { get; set; }
    }
}
